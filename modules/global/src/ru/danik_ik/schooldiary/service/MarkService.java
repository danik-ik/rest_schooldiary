package ru.danik_ik.schooldiary.service;


import java.util.UUID;

public interface MarkService {
    String NAME = "diary_MarkService";

    void deleteAllMarksForStudent(UUID studentId);
}