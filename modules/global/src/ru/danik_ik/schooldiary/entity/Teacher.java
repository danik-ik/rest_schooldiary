package ru.danik_ik.schooldiary.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;

@NamePattern("%s|description")
@Table(name = "DIARY_TEACHER")
@Entity(name = "diary$Teacher")
public class Teacher extends StandardEntity {
    private static final long serialVersionUID = -7485372165357685571L;

    @NotNull
    @Column(name = "DESCRIPTION", nullable = false)
    protected String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


}