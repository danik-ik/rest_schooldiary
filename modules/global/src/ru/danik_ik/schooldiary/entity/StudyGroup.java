package ru.danik_ik.schooldiary.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;

@NamePattern("%s|description")
@Table(name = "DIARY_STUDY_GROUP")
@Entity(name = "diary$StudyGroup")
public class StudyGroup extends StandardEntity {
    private static final long serialVersionUID = 2597050592214849201L;

    @NotNull
    @Column(name = "DESCRIPTION", nullable = false)
    protected String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


}