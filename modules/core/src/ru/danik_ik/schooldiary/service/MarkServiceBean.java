package ru.danik_ik.schooldiary.service;

import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.UUID;

@Service(MarkService.NAME)
public class MarkServiceBean implements MarkService {

    @Inject
    private Persistence persistence;

    @Override
    public void deleteAllMarksForStudent(UUID studentId) {
        try (Transaction tx = persistence.createTransaction()) {
            EntityManager em = persistence.getEntityManager();
            em.createQuery("delete from diary$Mark m where m.student.id = :studentId")
                    .setParameter("studentId", studentId)
                    .executeUpdate();
            tx.commit();
        }

    }
}