-- begin DIARY_STUDENT
create table DIARY_STUDENT (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    STUDY_GROUP_ID varchar(36),
    BORN_DATE date not null,
    PHONE_NUMBER varchar(255),
    --
    primary key (ID)
)^
-- end DIARY_STUDENT
-- begin DIARY_STUDY_GROUP
create table DIARY_STUDY_GROUP (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    DESCRIPTION varchar(255) not null,
    --
    primary key (ID)
)^
-- end DIARY_STUDY_GROUP
-- begin DIARY_SUBJECT
create table DIARY_SUBJECT (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    DESCRIPTION varchar(255),
    --
    primary key (ID)
)^
-- end DIARY_SUBJECT
-- begin DIARY_MARK_CAUSE
create table DIARY_MARK_CAUSE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    SUBJECT_ID varchar(36) not null,
    NAME varchar(255) not null,
    DESCRIPTION varchar(255),
    --
    primary key (ID)
)^
-- end DIARY_MARK_CAUSE
-- begin DIARY_MARK
create table DIARY_MARK (
    ID varchar(36) not null,
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    --
    GRADE integer not null,
    CAUSE_ID varchar(36) not null,
    STUDENT_ID varchar(36) not null,
    TEACHER_ID varchar(36) not null,
    DATE_ date not null,
    --
    primary key (ID)
)^
-- end DIARY_MARK
-- begin DIARY_TEACHER
create table DIARY_TEACHER (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    DESCRIPTION varchar(255) not null,
    --
    primary key (ID)
)^
-- end DIARY_TEACHER
