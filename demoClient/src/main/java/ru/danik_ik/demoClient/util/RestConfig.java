package ru.danik_ik.demoClient.util;

import java.util.Base64;

public class RestConfig {

    private final static String HTTP_USER_NAME = "client";
    private final static String HTTP_PASSWORD = "secret";
    private final static String APPLICATION_USER_NAME = "admin";
    private final static String APPLICATION_USER_PASSWORD = "admin";

    public final static String URL_BASE = "http://localhost:8080/app/rest/v2/";
    public final static String ENCODED_HTTP_AUTH_DATA =
            "Basic " + new String(Base64.getEncoder().encode((HTTP_USER_NAME + ":" + HTTP_PASSWORD).getBytes()));
    public final static String AUTH_REQUEST_BODY = "grant_type=password&username=" + APPLICATION_USER_NAME
            + "&password=" + APPLICATION_USER_PASSWORD;
    // предполагается, что имя и пароль не содержат символов, требующих экранирования. В идеале, надо кодировать
    // (httpEncode). Тут константы, заданные здесь же, поэтому не стал тащить лишнюю зависимость.
}
