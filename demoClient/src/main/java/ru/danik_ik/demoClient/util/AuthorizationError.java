package ru.danik_ik.demoClient.util;

public class AuthorizationError extends Exception {
    public AuthorizationError() {
    }

    public AuthorizationError(String message) {
        super(message);
    }

    public AuthorizationError(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthorizationError(Throwable cause) {
        super(cause);
    }
}
