package ru.danik_ik.demoClient.util;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.filter.log.LogDetail;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import static ru.danik_ik.demoClient.helpers.PrintHelper.printHeader;
import static ru.danik_ik.demoClient.util.RestConfig.*;

public class Authorization {
    private final static String URL_AUTH = "oauth/token";

    private final RequestSpecification requestSpecification = new RequestSpecBuilder()
            .setAccept(ContentType.JSON)
            .log(LogDetail.ALL)
            .build()
            ;

    private volatile UUID authorizationToken;

    public UUID getAuthorizationToken() throws AuthorizationError{
        if (authorizationToken == null) doAuthorizationRequest();
        return authorizationToken;
    }

    private synchronized void doAuthorizationRequest() throws AuthorizationError {
        // Double-check locking
        if (authorizationToken != null) return;

        String responseBody;
        Response response = null;
        try {
            printHeader("Authorization request:");
            response = getAuthorizationResponse();
            printHeader("Authorization response:");
            responseBody = response.getBody().asString();
            System.out.println(responseBody);
        } catch (Exception e) {
            throw new AuthorizationError("Authorization request error", e);
        }

        if (200 != response.getStatusCode()) throw new AuthorizationError(responseBody);

        try {
            JSONObject jo = new JSONObject(responseBody);
            authorizationToken = UUID.fromString(jo.getString("access_token"));
        } catch (JSONException e) {
            throw new AuthorizationError("Authorization response parsing error: " + responseBody, e);
        }
    }

    private Response getAuthorizationResponse() {
        RequestSpecification request = RestAssured.given(requestSpecification)
                .contentType("application/x-www-form-urlencoded; charset=utf-8")
                .header("Authorization", ENCODED_HTTP_AUTH_DATA)
                .body(AUTH_REQUEST_BODY)
                ;
        return request.post(URL_BASE + URL_AUTH);
    }

}
