package ru.danik_ik.demoClient.demo;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.filter.log.LogDetail;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.danik_ik.demoClient.util.Authorization;
import ru.danik_ik.demoClient.util.AuthorizationError;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import static ru.danik_ik.demoClient.helpers.PrintHelper.printSubHeader;
import static ru.danik_ik.demoClient.helpers.PrintHelper.printSubHeader2;
import static ru.danik_ik.demoClient.util.RestConfig.URL_BASE;

public class DataManipulator {
    protected final Authorization auth;
    protected RequestSpecification requestSpecification;
    private UUID token;

    public DataManipulator(Authorization auth) {
        this.auth = auth;
    }

    public void init() throws AuthorizationError {
        token = auth.getAuthorizationToken();
        requestSpecification = new RequestSpecBuilder()
                .setAccept(ContentType.JSON)
                .log(LogDetail.ALL)
                .addHeader("Authorization", "Bearer " + token)
                .build()
        ;
    }

    protected Response doRestQuery(String urlTail) throws RestError {
        return doRestQuery(urlTail, null);
    }

    protected Response doRestQuery(String urlTail, Consumer<RequestSpecification> tuner) throws RestError {
        RequestSpecification request = RestAssured.given(requestSpecification);
        if (tuner != null) tuner.accept(request);
        Response response = request.get(URL_BASE + urlTail);

        printSubHeader2("Response:");
        System.out.println(response.getBody().asString());
        checkResponseStatusCode(response, 200);
        return response;
    }

    protected UUID getOneId(String entityName) throws RestError {
        printSubHeader2("Getting one id for entity: " + entityName);
        Response response;
        JSONArray ja;
        response = doRestQuery("queries/" + entityName + "/all?view=_minimal&limit=1");
        ja = new JSONArray(response.getBody().asString());
        if (ja.length() < 1) throw new RestError("error: there are no entities for " + entityName + " in the database");

        JSONObject jo = ja.getJSONObject(0);
        return UUID.fromString(jo.getString("id"));
    }

    protected void getStudentById(UUID id) throws RestError {
        printSubHeader("getting student by id: " + id);

        Response response = doRestQuery("queries/diary$Student/byId?view=full",
                r -> r.param("id", id)
        );
    }

    protected void wrapException(RestAction action) {
        try {
            action.run();
        } catch (RestError restError) {
            restError.printStackTrace();
        }
    }

    @FunctionalInterface
    protected interface RestAction {
        void run() throws RestError;
    }

    protected List<UUID> getIdsFromJsonArray(String responseStr) {
        List<UUID> result = new ArrayList<>();
        JSONArray ja = new JSONArray(responseStr);
        for (int i = 0; i < ja.length(); i++) {
            JSONObject jo = ja.getJSONObject(i);
            result.add(UUID.fromString(jo.getString("id")));
        }
        return result;
    }

    protected void checkResponseStatusCode(Response response, int expectedCode) throws RestError {
        if (!(response.statusCode() == expectedCode))
            throw new RestError(String.format("Unexpected response status code: must be %d, but was %d",
                    expectedCode, response.getStatusCode()));
    }

    protected JSONObject getJsonObjectWithOnlyId(Object id) {
        JSONObject result = new JSONObject();
        result.put("id", id);
        return result;
    }

}
