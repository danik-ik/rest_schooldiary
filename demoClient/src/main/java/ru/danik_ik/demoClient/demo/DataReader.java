package ru.danik_ik.demoClient.demo;

import com.jayway.restassured.response.Response;
import ru.danik_ik.demoClient.util.Authorization;
import ru.danik_ik.demoClient.util.AuthorizationError;

import java.util.List;
import java.util.UUID;

import static ru.danik_ik.demoClient.helpers.PrintHelper.printHeader;
import static ru.danik_ik.demoClient.helpers.PrintHelper.printSubHeader;
import static ru.danik_ik.demoClient.helpers.PrintHelper.printSubHeader2;

public class DataReader extends DataManipulator {
    public DataReader(Authorization auth) {
        super(auth);
    }

    @Override
    public void init() throws AuthorizationError {
        super.init();
    }

    public void getStudents() {
        wrapException( () -> {
            printHeader("getting students list...");
            List<UUID> studentIds = getStudentIds();
            printSubHeader("IDs:");
            studentIds.forEach(System.out::println);

            for (UUID id: studentIds) getStudentById(id);
        });
    }

    private List<UUID> getStudentIds() throws RestError {

        Response response = doRestQuery("queries/diary$Student/all?view=_minimal");
        // можно entities/diary$Student, но будет больше данных

        return getIdsFromJsonArray(response.getBody().asString());
    }


    public void getMarksForOneStudent() {
        wrapException( () -> {
            printHeader("getting marks for one student");

            UUID studentId = getOneId("diary$Student");

            printSubHeader("Full:");
            getMarksForStudentFull(studentId);

            printSubHeader("Short:");
            getMarksForStudentShort(studentId);
        });
    }

    private void getMarksForStudentShort(UUID studentId) throws RestError {
        doRestQuery("queries/diary$Mark/forStudent?view=_minimal",
                r -> r.param("studentId", studentId)
        );
    }

    private void getMarksForStudentFull(UUID studentId) throws RestError {
        doRestQuery("queries/diary$Mark/forStudent",
                r -> r.param("studentId", studentId)
        );
    }

    public void getMarksForStudentAndSubject() {
        wrapException( () -> {
            printHeader("getting marks for one student, one subject");

            UUID studentId = getOneId("diary$Student");
            UUID subjectId = getOneId("diary$Subject");

            printSubHeader("Marks list:");
            doRestQuery("queries/diary$Mark/forStudentAndSubject",
                    r -> r.param("studentId", studentId)
                            .param("subjectId", subjectId)
            );
        });
    }

}
