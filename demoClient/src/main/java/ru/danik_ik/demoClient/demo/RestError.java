package ru.danik_ik.demoClient.demo;

public class RestError extends Exception {
    public RestError() {
    }

    public RestError(String message) {
        super(message);
    }

    public RestError(String message, Throwable cause) {
        super(message, cause);
    }

    public RestError(Throwable cause) {
        super(cause);
    }
}
