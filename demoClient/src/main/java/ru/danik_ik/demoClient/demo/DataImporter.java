package ru.danik_ik.demoClient.demo;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import ru.danik_ik.demoClient.helpers.TabbedTextHelper;
import ru.danik_ik.demoClient.util.Authorization;
import ru.danik_ik.demoClient.util.AuthorizationError;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import static ru.danik_ik.demoClient.helpers.PrintHelper.printHeader;
import static ru.danik_ik.demoClient.helpers.PrintHelper.printSubHeader;
import static ru.danik_ik.demoClient.helpers.PrintHelper.printSubHeader2;
import static ru.danik_ik.demoClient.util.RestConfig.URL_BASE;

public class DataImporter extends DataManipulator {

    public DataImporter(Authorization auth) {
        super(auth);
    }

    private final Import[] imports = new Import[]{
        new Import("/teacher.tab.txt", "diary$Teacher"),
        new Import("/studyGroup.tab.txt", "diary$StudyGroup"),
        new Import("/subject.tab.txt", "diary$Subject"),
        new Import("/markCause.tab.txt", "diary$MarkCause"),
        new Import("/student.tab.txt", "diary$Student"),
        new Import("/mark.tab.txt", "diary$Mark"),
    };

    public void doPosts() throws AuthorizationError {
        this.init();
        for (Import i: imports) i.execute();
    }

    private class Import {
        private String resourceName;
        private String entityName;

        public Import(String resourceName, String entityName) {
            this.resourceName = resourceName;
            this.entityName = entityName;
        }

        public void execute() {
            printHeader("Data import");

            List<Map<String, String>> rows = new TabbedTextHelper(Charset.forName("CP1251")).parseTabbedText(resourceName);

            for(Map<String, String> row: rows) {
                printSubHeader2("Entity name: " + entityName);
                wrapException(()->postEntity(row, entityName));
            }
        }

        private void postEntity(Map<String, String> row, String entityName) throws RestError {
            JSONObject jo = new JSONObject();
            for (Map.Entry<String, String> e: row.entrySet()) {
                String key = e.getKey();
                if (key == null || "".equals(key)) continue;

                if (key.charAt(0) == '@') {
                    jo.put(key.substring(1), getJsonObjectWithOnlyId(e.getValue()));
                } else {
                    jo.put(key, e.getValue());
                }
            }

            RequestSpecification request = RestAssured.given(requestSpecification)
                    .body(jo.toString())
                    .contentType(ContentType.JSON)
                    ;
            printSubHeader2("Request:");
            Response response = request.post(URL_BASE + "entities/" + entityName);
            printSubHeader2("Response:");
            System.out.println(response.getBody().asString());
            checkResponseStatusCode(response, 201);
        }
    }
}
