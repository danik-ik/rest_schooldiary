package ru.danik_ik.demoClient.demo;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import ru.danik_ik.demoClient.util.Authorization;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static ru.danik_ik.demoClient.helpers.PrintHelper.printHeader;
import static ru.danik_ik.demoClient.helpers.PrintHelper.printSubHeader;
import static ru.danik_ik.demoClient.helpers.PrintHelper.printSubHeader2;
import static ru.danik_ik.demoClient.util.RestConfig.URL_BASE;

public class DataUpdater extends DataManipulator {
    public DataUpdater(Authorization auth) {
        super(auth);
    }

    public void updateOneStudent() {
        wrapException( () -> {
            printHeader("Updating student: study group, phone number");
            UUID studentId = getOneId("diary$Student");
            UUID groupId = addStudyGroup();
            String phoneNumber = "02";
            updateStudent(studentId, groupId, phoneNumber);
            getStudentById(studentId);
        });
    }

    private UUID addStudyGroup() throws RestError {
        printSubHeader("Creating new group");
        JSONObject jo = new JSONObject();
        jo.put("description", "1 «Ь»");

        RequestSpecification request = RestAssured.given(requestSpecification)
                .body(jo.toString())
                .contentType(ContentType.JSON)
                ;
        printSubHeader2("Request:");
        Response response = request.post(URL_BASE + "entities/diary$StudyGroup");
        printSubHeader2("Response:");
        System.out.println(response.getBody().asString());
        checkResponseStatusCode(response, 201);

        jo = new JSONObject(response.getBody().asString());
        return UUID.fromString(jo.getString("id"));
    }

    private void updateStudent(UUID studentId, UUID groupId, String phoneNumber) throws RestError {
        printSubHeader("Updating student");
        JSONObject jo = new JSONObject();
        jo.put("studyGroup", getJsonObjectWithOnlyId(groupId));
        jo.put("phoneNumber", phoneNumber);

        RequestSpecification request = RestAssured.given(requestSpecification)
                .body(jo.toString())
                .contentType(ContentType.JSON)
                ;
        printSubHeader2("Request:");
        Response response = request.put(URL_BASE + "entities/diary$Student/" + studentId);
        printSubHeader2("Response:");
        System.out.println(response.getBody().asString());
        checkResponseStatusCode(response, 200);
    }

    public void deleteMarksAndSetAgain() {
        wrapException( () -> {
            printHeader("deleting all marks for student");
            UUID studentId = getOneId("diary$Student");
            printSubHeader("current marks for student");
            List<UUID> markIds = getMarksForStudent(studentId);
            if (markIds.isEmpty()) throw new RestError("There ara no marks for student");
            printSubHeader("deleting one mark");
            deleteMark(markIds.get(0));
            printSubHeader("new list of marks");
            getMarksForStudent(studentId);
            deleteAllMarksForStudent(studentId);
            printSubHeader("next list(must be empty)");
            getMarksForStudent(studentId);

            printHeader("Setting new marks again");
            UUID teacherId = getOneId("diary$Teacher");
            List<UUID> causes = getMarkCauseIds();
            for (UUID causeId: causes) {
                setNewMark(studentId, causeId, teacherId, 5);
            }

            printSubHeader("and marks are here again!");
            getMarksForStudent(studentId);

        });
    }

    private void setNewMark(UUID studentId, UUID causeId, UUID teacherId, int grade) throws RestError {
        printSubHeader("Creating new mark");
        JSONObject jo = new JSONObject();
        jo.put("student", getJsonObjectWithOnlyId(studentId));
        jo.put("teacher", getJsonObjectWithOnlyId(teacherId));
        jo.put("cause", getJsonObjectWithOnlyId(causeId));
        jo.put("grade", grade);
        jo.put("date", LocalDate.now());

        RequestSpecification request = RestAssured.given(requestSpecification)
                .body(jo.toString())
                .contentType(ContentType.JSON)
                ;
        printSubHeader2("Request:");
        Response response = request.post(URL_BASE + "entities/diary$Mark");
        printSubHeader2("Response:");
        System.out.println(response.getBody().asString());
        checkResponseStatusCode(response, 201);
    }

    private List<UUID> getMarkCauseIds() throws RestError {
        Response response = doRestQuery("queries/diary$MarkCause/all?view=_minimal");
        return getIdsFromJsonArray(response.getBody().asString());
    }

    private void deleteAllMarksForStudent(UUID studentId) throws RestError {
        printSubHeader("Deleting ALL marks for student " + studentId);
        RequestSpecification request = RestAssured.given(requestSpecification)
                .param("studentId", studentId)
                ;
        printSubHeader2("Request:");
        Response response = request.get(URL_BASE + "services/diary_MarkService/deleteAllMarksForStudent");
        printSubHeader2("Response:");
        System.out.println(response.getBody().asString());
        checkResponseStatusCode(response, 204);
    }

    private void deleteMark(UUID uuid) throws RestError {
        RequestSpecification request = RestAssured.given(requestSpecification);
        printSubHeader2("Request:");
        Response response = request.delete(URL_BASE + "entities/diary$Mark/" + uuid);
        printSubHeader2("Response:");
        System.out.println(response.getBody().asString());
        checkResponseStatusCode(response, 200);
    }

    private List<UUID> getMarksForStudent(UUID studentId) throws RestError {

        Response response = doRestQuery("queries/diary$Mark/forStudent?view=_minimal",
                r -> r.param("studentId", studentId)
        );

        return getIdsFromJsonArray(response.getBody().asString());
    }

}
