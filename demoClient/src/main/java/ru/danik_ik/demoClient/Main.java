package ru.danik_ik.demoClient;

import ru.danik_ik.demoClient.demo.DataImporter;
import ru.danik_ik.demoClient.demo.DataReader;
import ru.danik_ik.demoClient.demo.DataUpdater;
import ru.danik_ik.demoClient.util.Authorization;
import ru.danik_ik.demoClient.util.AuthorizationError;

public class Main
{
    public static void main( String[] args ) throws AuthorizationError {
        Authorization auth = new Authorization();

        if (!(args.length > 0 && "skipImport".equalsIgnoreCase(args[0])))
            new DataImporter(auth).doPosts();

        DataReader dr = new DataReader(auth);
        dr.init();
        dr.getStudents();
        dr.getMarksForOneStudent();
        dr.getMarksForStudentAndSubject();

        DataUpdater du = new DataUpdater(auth);
        du.init();
        du.updateOneStudent();
        du.deleteMarksAndSetAgain();

    }
}
