package ru.danik_ik.demoClient.helpers;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.danik_ik.demoClient.helpers.ResourceHelper.getResourceLines;

public class TabbedTextHelper {
    private final Charset cs;

    public TabbedTextHelper(Charset cs) {
        this.cs = cs;
    }

    public List<Map<String, String>> parseTabbedText(String resourceName) {
        String headersLine = getResourceLines(resourceName, cs)
                .findFirst()
                .get()
                ;
        String[] headers = headersLine.split("\t", -1);

        return getResourceLines(resourceName, cs)
                .skip(1)
                .filter(s -> !s.trim().isEmpty())
                .map((line) -> getRowMap(headers, line.split("\t", -1)))
                .collect(Collectors.toList());
    }

    private Map<String, String> getRowMap(String[] headers, String[] dataRow) {
        Map<String, String> result = new HashMap<>();
        for (int i= 0; i < headers.length; i++) {
            if (i <= dataRow.length && !"".equals(dataRow[i]))
                result.put(headers[i], dataRow[i]);
        }
        return result;
    }
}
