package ru.danik_ik.demoClient.helpers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.stream.Stream;

public class ResourceHelper {
    public static String getResourceAsString(String file_name) {
        return convertStreamToString(ResourceHelper.class.getResourceAsStream(file_name));
    }

    public static String getResourceAsString(String file_name, Class BaseClass) {
        return convertStreamToString(BaseClass.getResourceAsStream(file_name));
    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is, "UTF-8").useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public static Stream<String> getResourceLines(String resourceName, Charset cs) {
        return getResourceLines(resourceName, cs, ResourceHelper.class);
    }

    public static Stream<String> getResourceLines(String resourceName, Charset cs, Class baseClass) {
        return new BufferedReader(new InputStreamReader(baseClass.getResourceAsStream(resourceName), cs))
                .lines();
    }
}
