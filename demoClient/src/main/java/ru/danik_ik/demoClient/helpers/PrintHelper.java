package ru.danik_ik.demoClient.helpers;

public class PrintHelper {
    public static void printHeader(String header) {
        System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n>>> "
                + header.toUpperCase() + "\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
    }

    public static void printSubHeader(String header) {
        System.out.println("\n>>> " + header + "\n");
    }
    public static void printSubHeader2(String header) {
        System.out.println("> " + header);
    }
}
